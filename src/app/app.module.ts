import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { MaterialModule } from './material/material.module';

/* Main Components */
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CartComponent } from './components/cart/cart.component';
import { CategorySliderComponent } from './components/category-slider/category-slider.component';
import { CartProductComponent } from './components/cart-product/cart-product.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CategoryFormComponent } from './components/category-form/category-form.component';
import { ProductFormComponent } from './components/product-form/product-form.component';
import { LoginComponent } from './components/login/login.component';

/* Locale configuration */
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';

/* Services */
import { InventoryService } from './services/inventory.service';
import { SidenavService } from './services/sidenav.service';
import { CartService } from './services/cart.service';
import { UserService } from './services/user.service';

/* Router */
import { AppRoutingModule } from './app-routing.module';

/* Directivas para poder leer correctamente el base64 de la imagen de un producto: 
https://stackoverflow.com/questions/41889384/angular2-validation-for-input-type-file-wont-trigger-when-changing-the-fi/41938495#41938495 */
import { FileValueAccessor } from './directives/file-control-value-accessor';
import { FileValidator } from './directives/file-input.validator';

registerLocaleData(localeEs); //para poder mostrar el símbolo € a la derecha de los precios usando el pipe currency
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CartComponent,
    CategorySliderComponent,
    CartProductComponent,
    CategoriesComponent,
    LoginComponent,
    CategoryFormComponent,
    ProductFormComponent,
    FileValueAccessor,
    FileValidator
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CarouselModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es-ES' },
    InventoryService,
    CartService,
    UserService,
    SidenavService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
