import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Category } from '../models/category.model';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  categories: Category[] = [
    {
      id:"1",
      name: 'Smartphones',
    },
    {
      id:"2",
      name: 'Smartwatches',
    },
    {
      id:"3",
      name: 'Televisores',
    },
  ]

  products: Product[] = [
    {
      id: '1',
      name: 'Apple iPhone 12 Pro Max',
      description: '128GB de memoria, iOS, Chip A14 Bionic, 6.7 pulgadas',
      stock: 0,
      price: 1250,
      image_path: 'assets/images/telefonos/iphone12promax.webp',
      category: '1',
    },
    {
      id: '2',
      name: 'Samsung Galaxy A32',
      description:
        '128GB de memoria, Android, procesador octa-core, 6.4 pulgadas',
      stock: 1,
      price: 239,
      image_path: 'assets/images/telefonos/samsunga51.webp',
      category: '1',
    },
    {
      id: '3',
      name: 'ZTE Blade A51',
      description:
        '32GB de memoria, Android, procesador SC9863A octa-core, 6.52 pulgadas',
      stock: 2,
      price: 100,
      image_path: 'assets/images/telefonos/ztebladea51.webp',
      category: '1',
    },
    {
      id: '4',
      name: 'Xiaomi Redmi Note 10',
      description:
        '128GB de memoria, Android, MediaTek Dimensity 700, 6.5 pulgadas',
      stock: 2,
      price: 229,
      image_path: 'assets/images/telefonos/xiaomiredmi9.webp',
      category: '1',
    },
    {
      id: '5',
      name: 'OPPO A91',
      description:
        '128GB de memoria, Android, MediaTek Helio P70, 6.4 pulgadas',
      stock: 2,
      price: 229,
      image_path: 'assets/images/telefonos/oppo.webp',
      category: '1',
    },
    {
      id: '6',
      name: 'Apple Watch Series 6',
      description:
        '40mm, WatchOS 7, pantalla OLED, 32GB de memoria, GPS y resistente al agua',
      stock: 2,
      price: 421,
      image_path: 'assets/images/relojes/apple_watch_6.webp',
      category: '2',
    },
    {
      id: '7',
      name: 'Apple Watch SE',
      description:
        '44mm, WatchOS 7, pantalla OLED, 32GB de memoria, GPS y resistente al agua',
      stock: 2,
      price: 329,
      image_path: 'assets/images/relojes/apple_watch_se.webp',
      category: '2',
    },
    {
      id: '8',
      name: 'AmazFit GTS',
      description:
        'Compatible con iOS y Android. Pantalla OLED, Bluetooth, brújula y sumergible',
      stock: 2,
      price: 89.9,
      image_path: 'assets/images/relojes/amazfit_gts.webp',
      category: '2',
    },
    {
      id: '9',
      name: 'Samsung Galaxy Watch Active 2',
      description:
        'Compatible con Android. Tizen OS 4.x, pantalla Super AMOLED, con sensor de localización GPS y resistente al agua',
      stock: 2,
      price: 199,
      image_path: 'assets/images/relojes/galaxy_watch.webp',
      category: '2',
    },
    {
      id: '10',
      name: 'Huawei Watch GT2 Pro',
      description:
        'Pantalla AMOLED, sumergible, bluetooth, sensor de movimiento',
      stock: 2,
      price: 209,
      image_path: 'assets/images/relojes/huawei_watch.webp',
      category: '2',
    },
    {
      id: '11',
      name: 'TCL 55C722',
      description: '55 pulgadas, UDH 4K, 60Hz, HDR10, Android TV, Dolby Atmos',
      stock: 2,
      price: 749,
      image_path: 'assets/images/tvs/TCL55c722.webp',
      category: '3',
    },
    {
      id: '12',
      name: 'Grundig 55 GFU 7960B',
      description: '55 pulgadas, UDH 4K, 50Hz, HDR, Chromecast integrado',
      stock: 2,
      price: 563,
      image_path: 'assets/images/tvs/grundig55gfu.webp',
      category: '3',
    },
    {
      id: '13',
      name: 'Sony KD55X81JAEP',
      description: '55 pulgadas, UDH 4K, 50Hz, HDR10, Google TV',
      stock: 2,
      price: 929,
      image_path: 'assets/images/tvs/sony_kd55.webp',
      category: '3',
    },
    {
      id: '14',
      name: 'LG OLED 55BX6LB',
      description:
        '55 pulgadas, UDH 4K, 50/60Hz, HDR10 Pro, WebOS 5.0, Dolby Atmos',
      stock: 2,
      price: 989,
      image_path: 'assets/images/tvs/lgoled55bx.webp',
      category: '3',
    },
    {
      id: '15',
      name: 'Philips 58PUS7555',
      description: '58 pulgadas, UDH 4K, 50/60Hz, HDR10+, Linux, Dolby Atmos',
      stock: 2,
      price: 529,
      image_path: 'assets/images/tvs/philips_58pus.webp',
      category: '3',
    },
  ];

  private productsSubject = new BehaviorSubject<Product[]>(this.products);
  products$ = this.productsSubject.asObservable();

  constructor() { }

  getAllProducts() : Product[]{
    return this.products;
  }

  getAllCategories() : Category[] {
    return this.categories;
  }

  getProduct(id: string) : Product{
    return this.products.find(product => id === product.id);
  }

  getCategory(id: string) : Category {
    return this.categories.find(category => id === category.id);
  }

  getCategoryProducts(categoryId: string) : Product[]{
    return this.products.filter(product => categoryId === product.category)
  }

  createCategory(category: Category): void {
    this.categories.push(category);
    console.log('Create Category called')
    console.log(this.categories)
  }

  createProduct(product: Product) : void {
    this.products = [...this.products, product]
    this.productsSubject.next(this.products);
    console.log('Create Product called')
    console.log(this.products)
  }

  getNextProductId(): string {
    return this.products.length === 0 ? "1" : (parseInt(this.products[this.products.length - 1].id) + 1).toString();
  }

  getNextCategoryId(): string {
    return this.categories.length === 0 ? "1" : (parseInt(this.categories[this.categories.length - 1].id) + 1).toString();
  }

  increaseProductStock(id: string, stock = 1){
    let product = this.getProduct(id);
    product.stock += stock;
  }

  decreaseProductStock(id: string, stock = 1){
    let product = this.getProduct(id);
    product.stock -= stock;
  }
}
