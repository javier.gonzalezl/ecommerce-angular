import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CartProduct } from '../models/cart-product.model';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private products: CartProduct[] = [];
  private cart = new BehaviorSubject<CartProduct[]>([]);

  cart$ = this.cart.asObservable();

  constructor() {}

  getAllProducts() {
    return this.products;
  }

  cartIsEmpty(): boolean {
    return this.products.length === 0;
  }

  productIsInCart(id: string): boolean {
    return this.products.some((product) => product.id === id);
  }

  getProductFromCart(id: string): CartProduct {
    return this.products.find((product) => product.id === id);
  }

  createCartProductFromProduct(product: Product): CartProduct {
    return {
      id: product.id,
      name: product.name,
      description: product.description,
      quantity: 1,
      price: product.price,
      image_path: product.image_path,
    };
  }

  addToCart(product: Product): void {
    if (!this.cartIsEmpty() && this.productIsInCart(product.id)) {
      let cartProduct = this.getProductFromCart(product.id);
      cartProduct.quantity++;
    } else {
      this.products = [
        ...this.products,
        this.createCartProductFromProduct(product),
      ];
    }

    console.log(this.products);
    this.cart.next(this.products);
  }

  increaseQuantity(id: string){
    let cartProduct = this.getProductFromCart(id);
    cartProduct.quantity++;
    this.cart.next(this.products);
  }

  decreaseQuantity(id: string){
    let cartProduct = this.getProductFromCart(id);
    cartProduct.quantity--;
    this.cart.next(this.products);
  }

  deleteFromCart(id: string): void {
    let productsUpdated = this.products.filter(product => product.id !== id)
    this.products = [...productsUpdated];
    this.cart.next(this.products);
  }

  emptyCart(): void {
    this.products = [];
    this.cart.next(this.products)
  }
}
