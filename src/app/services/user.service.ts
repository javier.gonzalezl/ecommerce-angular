import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  users: User[] = [
    {
      username: 'admin',
      password: 'admin',
      role: 'admin',
    },
    {
      username: 'guest',
      password: 'guest',
      role: 'guest',
    },
  ];
  private userLogged: User = null;

  private userSubject: BehaviorSubject<User>;
  user$: Observable<User>;

  constructor(private router: Router) {
    this.userSubject = new BehaviorSubject<User>(
      JSON.parse(localStorage.getItem('user'))
    );
    this.user$ = this.userSubject.asObservable();
  }

  getUserByUsername(username: string) {
    return this.users.find((user) => user.username === username.toLowerCase());
  }

  login(username: string, password: string) {
    if (username) {
      this.userLogged = this.getUserByUsername(username);
      if (this.userLogged && this.userLogged.password === password) {
        localStorage.setItem('user', JSON.stringify(this.userLogged));
        this.userSubject.next(this.userLogged);
        return this.userLogged;
      }
    }
    return false;
  }

  logout() {
    localStorage.removeItem('user');
    this.userSubject.next(null);
  }
}
