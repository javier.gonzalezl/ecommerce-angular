import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileValidator } from 'src/app/directives/file-input.validator';
import { Category } from 'src/app/models/category.model';
import { InventoryService } from 'src/app/services/inventory.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  form: FormGroup;
  imgFile: string;
  categories: Category[];

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private inventoryService: InventoryService,
    private snackBar: MatSnackBar
  ) { 
    this.buildForm();
  }

  ngOnInit(): void {
    this.getCategories();
  }

  createProduct(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.inventoryService.createProduct({
        'id': this.inventoryService.getNextProductId(),
        'name': value.name,
        'description': value.description,
        'stock': value.stock,
        'price': value.price,
        'image_path': value.image_path,
        'category': value.category,
      });
      this.openSnackBar('Producto creado.');
      this.closeDialog();
    }
  }

  onCancelClick(): void {
    this.closeDialog();
  }

  
  private buildForm() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      stock: ['1', [Validators.required]],
      price: ['', [Validators.required]],
      image_path: ['', [FileValidator.validate]],
      category: ['', [Validators.required]],
    });
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'CERRAR')
  }

  private getCategories(){
    this.categories = this.inventoryService.getAllCategories();
  }

  onImageChange(e) {
    const reader = new FileReader();
    
    if(e.target.files && e.target.files.length) {
      const [file] = e.target.files;
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.imgFile = reader.result as string;
        this.form.patchValue({
          image_path: reader.result
        });
   
      };
    }
  }

  closeDialog(): void{
    this.dialog.closeAll();
  }

}
