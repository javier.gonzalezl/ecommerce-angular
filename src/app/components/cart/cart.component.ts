import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map  } from "rxjs/operators";
import { CartProduct } from 'src/app/models/cart-product.model';
import { CartService } from 'src/app/services/cart.service';
import { InventoryService } from 'src/app/services/inventory.service';
import { SidenavService } from 'src/app/services/sidenav.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {;
  cartProducts$: Observable<CartProduct[]>;
  total$:Observable<number>;

  constructor(
    private sidenav: SidenavService,
    private cartService: CartService,
    private inventoryService: InventoryService,
    private snackBar: MatSnackBar
  ) {
    this.cartProducts$ = this.cartService.cart$
    this.total$ = this.cartService.cart$.pipe(
      map(products => products.reduce((total,product) => total + product.quantity * product.price, 0))
    )
  }

  ngOnInit(): void {
  }

  toggleCartSidenav() {
    this.sidenav.toggle();
  }

  increaseProductQuantity(cartProduct: CartProduct){
    this.inventoryService.decreaseProductStock(cartProduct.id)
    this.cartService.increaseQuantity(cartProduct.id);
  }

  decreaseProductQuantity(cartProduct: CartProduct){
    this.inventoryService.increaseProductStock(cartProduct.id)
    this.cartService.decreaseQuantity(cartProduct.id);
  }

  removeProductFromCart(cartProduct: CartProduct){
    this.inventoryService.increaseProductStock(cartProduct.id, cartProduct.quantity)
    this.cartService.deleteFromCart(cartProduct.id);
  }

  isAddBtnDisabled(id: string){
    return this.inventoryService.getProduct(id).stock === 0;
  }

  checkout(){
    this.cartService.emptyCart();
    this.openSnackBar('Su pedido se ha tramitado con éxito. Gracias por la compra');

  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'CERRAR')
  }
}
