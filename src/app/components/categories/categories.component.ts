import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { InventoryService } from 'src/app/services/inventory.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  categories: Category[] = [];

  constructor(
    private inventoryService: InventoryService
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.categories = this.inventoryService.getAllCategories();
  }

}
