import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map  } from "rxjs/operators";
import { User } from 'src/app/models/user.model';
import { CartService } from 'src/app/services/cart.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { UserService } from 'src/app/services/user.service';
import { CategoryFormComponent } from '../category-form/category-form.component';
import { LoginComponent } from '../login/login.component';
import { ProductFormComponent } from '../product-form/product-form.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  user$: Observable<User>;
  total$: Observable<number>

  constructor(
    private sidenav: SidenavService,
    private userService: UserService,
    private cartService: CartService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar) { 
      this.total$ = this.cartService.cart$.pipe(
        map(products => products.reduce((total,product) => total + product.quantity, 0))
      )
      this.loadUserData();
    }

  toggleCartSidenav() {
    this.sidenav.toggle();
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '250px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openCreateCategoryDialog(): void {
    const dialogRef = this.dialog.open(CategoryFormComponent, {
      width: '250px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openCreateProductDialog(): void {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '400px', disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  loadUserData() {
    this.user$ = this.userService.user$
  }

  logout() {
    this.userService.logout();
    this.openSnackBar('Ha cerrado sesión con éxito.');
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'CERRAR')
  }



}
