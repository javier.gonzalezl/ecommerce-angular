import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { CartProduct } from 'src/app/models/cart-product.model';

@Component({
  selector: 'app-cart-product',
  templateUrl: './cart-product.component.html',
  styleUrls: ['./cart-product.component.scss']
})
export class CartProductComponent implements OnInit {

  @Input() cartProduct: CartProduct;
  @Input() disableAdd: boolean
  @Output() increaseButtonClicked: EventEmitter<CartProduct> = new EventEmitter();
  @Output() decreaseButtonClicked: EventEmitter<CartProduct> = new EventEmitter();
  @Output() deleteButtonClicked: EventEmitter<CartProduct> = new EventEmitter();
  
  constructor() { }

  ngOnInit(): void {
  }

  increaseQuantity(id: string) {
    this.increaseButtonClicked.emit(this.cartProduct);
  }

  decreaseQuantity(id: string) {
    this.decreaseButtonClicked.emit(this.cartProduct);
  }

  deleteFromCart(id: string) {
    this.deleteButtonClicked.emit(this.cartProduct);
  }

}
