import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { InventoryService } from 'src/app/services/inventory.service';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  form: FormGroup;

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private inventoryService: InventoryService,
    private snackBar: MatSnackBar
  ) {
      this.buildForm();
   }

  ngOnInit(): void {
  }

  createCategory(event: Event) {
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      this.inventoryService.createCategory({
        'id': this.inventoryService.getNextCategoryId(),
        'name': value.category,
      });
      this.openSnackBar('Categoría creada.');
      this.closeDialog();
    }
  }

  onCancelClick(): void {
    this.closeDialog();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      category: ['', [Validators.required]],
    });
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'CERRAR')
  }

  closeDialog(): void{
    this.dialog.closeAll();
  }

}
