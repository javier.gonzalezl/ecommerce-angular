import { Component, Input, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category.model';
import { Product } from 'src/app/models/product.model';
import { InventoryService } from 'src/app/services/inventory.service';

import { OwlOptions } from 'ngx-owl-carousel-o';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CartService } from 'src/app/services/cart.service';
import { SidenavService } from 'src/app/services/sidenav.service';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user.model';
@Component({
  selector: 'app-category-slider',
  templateUrl: './category-slider.component.html',
  styleUrls: ['./category-slider.component.scss'],
})
export class CategorySliderComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: false,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    freeDrag: true,
    dots: true,
    navSpeed: 700,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 2,
      },
      992: {
        items: 3,
      },
    },
  };

  @Input() category: Category;
  collapsed: boolean;
  products$: Observable<Product[]>;
  user$: Observable<User>;

  constructor(
    private inventoryService: InventoryService,
    private cartService: CartService,
    private sidenav: SidenavService,
    private userService: UserService
  ) {
    this.products$ = this.inventoryService.products$.pipe(
      map((products) =>
        products.filter((product) => product.category === this.category.id)
      )
    );

    this.loadUserData();
  }

  ngOnInit(): void {
    this.collapsed = false;
  }

  toggleCollapsed() {
    this.collapsed = !this.collapsed;
  }

  addToCart(id: string) {
    let product = this.inventoryService.getProduct(id);
    this.cartService.addToCart(product);
    product.stock--;
    this.sidenav.open();
  }

  loadUserData() {
    this.user$ = this.userService.user$
  }
}
