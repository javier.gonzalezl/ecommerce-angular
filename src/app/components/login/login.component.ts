import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  hide = true; // flag para controlar si se muestra la contraseña (por defecto, contraseña ocultada)

  constructor(
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private userService: UserService,
    private snackBar: MatSnackBar
  ) {
    this.buildForm();
   }

  ngOnInit(): void {
  }

  login(event: Event) {
    let message = '';
    event.preventDefault();
    if (this.form.valid) {
      const value = this.form.value;
      const result = this.userService.login(value.username, value.password);
      if (result) {
        message = 'Ha iniciado sesión con éxito.';
        this.closeDialog();
      } else{
        message = 'El usuario o la contraseña son incorrectos, vuelva a intentarlo.';
      }
      this.form.reset({username: '', password: ''});
      this.openSnackBar(message);
    }
  }

  onCancelClick(): void {
    this.closeDialog();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  private openSnackBar(message: string) {
    this.snackBar.open(message, 'CERRAR')
  }

  closeDialog(): void{
    this.dialog.closeAll();
  }

}
